﻿using System;
using System.Collections.Generic;
using SablonaNakup.Hraci;
using SablonaNakup;
using System.Diagnostics.Contracts;

namespace SablonaNakup.Hraci
{

    class Jednicka : Nakupujici
    {
        int x = 0;
        int y = 0;
        int chluptest = 0;
        int pozicetestx = 0;
        int pozicetesty = 0;
        int pohyb = 0;
        int Nahodax;
        int Nahoday;
        int JduzX = 0;  //abych se nevracel, zbytecny krok
        int JduzY = 0; //abych se nevracel, zbytecny krok

        public int Randomizerx(int mojesirka){
            Nahodax = nahoda.Next(5);


            while (Nahodax == JduzX){
                if (mojesirka <20){
                    Nahodax = nahoda.Next(5);
                    Nahodax = Nahodax + 1;
                }
                if (mojesirka > 60)
                {
                    Nahodax = nahoda.Next(5);
                    Nahodax = Nahodax - 1;
                }
                else{
                    Nahodax = nahoda.Next(5);

                }
            }
            return Nahodax;

        }
        public int Randomizery(int mojevyska)
        {
            Nahoday = nahoda.Next(5);


            while (Nahoday == JduzY)
            {
                if (mojevyska < 5)
                {
                    Nahoday = nahoda.Next(5);
                    Nahoday = Nahoday + 1;
                }
                if (mojevyska > 15)
                {
                    Nahoday = nahoda.Next(5);
                    Nahoday = Nahoday - 1;
                }
                else
                {
                    Nahoday = nahoda.Next(5);

                }
            }
            return Nahoday;

        }
        public int Krucekx(int x1)
        {
            if (x1 > 2)
            {
                x = 1;
            }
            if (x1 == 2){
                x = 0;
            }

            if (x1 < 2)
            {
                x = -1;
            }
           
            JduzX = x * -1;
            return x;

        }

        public int Kruceky(int y2) {
            if (y2 > 2)
            {
                y = 1;
            }
            if (y2 == 2)
            {
                y = 0;
            }

            if (y2 < 2)
            {
                y = -1;
            }
            JduzY = y * -1;
            return y;
        }
        public (int, int) Test2(int mysirka, int myvyska, int smersirka, int smervyska)
        {
            if (mysirka == 0 && smersirka == -1)
            {
                pozicetestx = 1;
            }
            if (mysirka == 79 && smersirka == 1)
            {
                pozicetestx = 1;
            }
            if(mysirka >0 && mysirka <79) {
                pozicetestx = 0;}

            if (myvyska == 0 && smervyska == -1)
            {

                pozicetesty = 1;
            }
            if (myvyska == 19 && smervyska == 1)
            {

                pozicetesty = 1;
            }
            if(myvyska > 0 && myvyska < 19)
            {
                pozicetesty = 0;

            }
            return (pozicetestx, pozicetesty);
        }

        public (int Nahodax, int Nahoday) Zmena (int zmenax,int zmenay){
            if (zmenax == 1) { zmenax = 3; }
            if (zmenax == -1) { zmenax = 1; }
            if (zmenax == 0) { zmenax = 2; }
            if (zmenay == 1) { zmenay = 3; }
            if (zmenay ==-1) { zmenay = 1; }
            if (zmenay == 0) { zmenay = 2; }
            Nahodax = zmenax;
            Nahoday = zmenay;
            return (Nahodax, Nahoday);
        } //z smeru najde bod pro mapku kam chci jít
        public override Smer Krok(char[,] castMapy, int penize, Souradnice mojeSouradnice) //udelani kroku
        {
            x = 0;
            y = 0;
            int penizeMam = penize;
            int Mojesirka = mojeSouradnice.X;
            int Mojevyska = mojeSouradnice.Y;
            int PocetNabidek = 0;
            int pocetzakazanychpoli = 0;
            List<int> xzakazane = new List<int>();
            List<int> yzakazane = new List<int>();
            List<int> sirkapoklad = new List<int>();
            List<int> vyskapoklad = new List<int>();
            List<int> hodnotapokladu = new List<int>();
            int nahodnyx = nahoda.Next(3) - 1;
            int nahodnyy = nahoda.Next(3) - 1;
            for (int vyska = 0; vyska <= 4; vyska++) //prohledam vsude kde vidim abych nasel prvni nejlepsi nabidku
            {
                for (int sirka = 0; sirka <= 4; sirka++)
                {   if( sirka == 2 && vyska == 2)
                    {
                        //nothing
                    }
                else {
                    char foo = castMapy[sirka, vyska];
                    int hodnotapole = foo - '0';
                    if (hodnotapole > 0 && hodnotapole < 10)
                    {
                        hodnotapokladu.Insert(PocetNabidek, hodnotapole);
                        sirkapoklad.Insert(PocetNabidek, sirka);
                        vyskapoklad.Insert(PocetNabidek, vyska);
                        PocetNabidek = PocetNabidek + 1;//do paměti jsem si ulozil kde poklad lezi

                    }
                    if (hodnotapole > 16 && hodnotapole < 44 || hodnotapole > 48 && hodnotapole <= 75){
                        xzakazane.Insert(pocetzakazanychpoli, sirka);
                        yzakazane.Insert(pocetzakazanychpoli, vyska);
                        pocetzakazanychpoli = pocetzakazanychpoli + 1;

                        //vytvařím místa, kam nesmím, protože je tam bud stena nebo zloděj
                        //Funguje.
 
                    }
                    else{
                        //nothing
                    }
                }
                }
            }     
//prohledal jsem, vim tedy co je kolem - pristupuji k rozhodovani
            if(PocetNabidek > 0){ 
                int docasnyz = 0;
                int l = 0;
                int z = hodnotapokladu[l];
                for (int i = 0; i < PocetNabidek; i++)
                {
                    docasnyz = hodnotapokladu[i];
                    if (docasnyz < z)
                    {
                        z = docasnyz;
                        l = i;
                    }
                }
                if (penizeMam>=z){
                x = sirkapoklad[l];
                y = vyskapoklad[l];
                }
                if (penizeMam <z){
                    Randomizerx(Mojesirka);
                    x = Nahodax;
                    Randomizery(Mojevyska);
                    y = Nahoday;
                }
                Krucekx(x);
                Kruceky(y);

                return new Smer((sbyte)(x), (sbyte)(y));
            }
            else{
                if (pohyb == 1)
                {

                    chluptest = 0;
                    int x3 = JduzX * -1;
                    int y3 = JduzY * -1;
                    Zmena(x3, y3);
                    Test2(Mojesirka, Mojevyska, x3, y3);
                    //detekce
                    for (int i = 0; i < pocetzakazanychpoli; i++)
                    {
                        if (xzakazane[i] == Nahodax && yzakazane[i] == Nahoday)
                        {
                            chluptest = 1;

                        }
                    }

                    if (chluptest == 1)
                    {
                        Randomizerx(Mojesirka);
                        x = Nahodax;
                        Randomizery(Mojevyska);
                        y = Nahoday;
                        Krucekx(x);
                        Kruceky(y);
                        Zmena(x, y);
                        pohyb = 0;
                        chluptest =0;
                    }
                    if (chluptest == 0){
                        x = x3;
                        y = y3;
                    }
                    if (pozicetestx == 1)
                    {
                        x = x * -1;
                    }
                    if (pozicetesty == 1)
                    {
                        y = y * -1;
                    }
                    JduzX = x * -1;
                    JduzY = y * -1;
                    return new Smer((sbyte)(x), (sbyte)(y));
                }
                if (pohyb == 0)
                {
                Startovac:
                    chluptest = 0;
                    Randomizerx(Mojesirka);
                    x = Nahodax;
                    Randomizery(Mojevyska);
                    y = Nahoday;
                    Krucekx(x);
                    Kruceky(y);
                    Zmena(x, y);
                    Test2(Mojesirka, Mojevyska, x, y);
                    //detekce 
                    for (int i = 0; i < pocetzakazanychpoli; i++)
                    {
                        if (xzakazane[i]==Nahodax && yzakazane[i]==Nahoday){
                            chluptest = 1;
                          
                        }
                    }
                    if (chluptest == 1)
                    {
                        goto Startovac;
                    }
                    if (pozicetestx == 1)
                    {
                        x = x * -1;
                    }
                    if (pozicetesty == 1)
                    {
                        y = y * -1;
                    }
                    pohyb = 1;
                    return new Smer((sbyte)(x), (sbyte)(y));
                }
                return new Smer((sbyte)(x), (sbyte)(y));
            }

         



        
    }
}
}